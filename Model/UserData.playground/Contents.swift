//: Playground - noun: a place where people can play

import Foundation
import UIKit
import Firebase
import Kronos

final class AccountUser
{
    var id : Int!
    var first_name: String?
    var last_name : String?
    var imageUser : String?
    
    var currentRange  : String?                 //id question from Firebase
    var minRange : String?                      //Min range Question
    var lastTimeVisited : String?               //server time
    var isItLive : Bool?                        // is it in app
    var waitTime : Double?                      // wait for time to get question
    var currentTime = Date ()                   //User Log In Time
    
    
    init(){}
    
    init (json : [String:Any])
    {
        self.id = json["id"] as! Int
        self.first_name = json["first_name"] as? String ?? ""
        self.last_name = json["last_name"] as? String ?? ""
        self.imageUser = json["photo_200"] as? String ?? ""
    }
    
    //MARK: Fetch User Information from Firebase
    func fetchUserFirebase (data : [String : Any])
    {
        self.currentRange = data["currentRange"] as? String ?? ""
        self.minRange = data["minRange"] as? String ?? ""
        self.lastTimeVisited = data["lastTimeVisited"] as? String ?? ""
        self.isItLive = data["isItLive"] as? Bool ?? false
        self.waitTime = data["WaitTime"] as? Double ?? 0.0
        
        // SYNC Date Time Monotonic
        syncDateTimeNow { (_ ) in }
    }
    
    
    //return Full Name user
    func FullName() -> String {
        return "\(first_name!) \(last_name!)"
    }
    //Saving last question user was
    func saveCurrentRangeQuestion (range : String)
    {
        DataService.instance.REF_USERS.child("\(id!)").child("currentRange").setValue(range)
    }
    
    //after completed specific question set current range question to max range
    func updateMinRange(range : String, completed : @escaping (Bool) -> Void)
    {
        DataService.instance.REF_USERS.child("\(id!)").child("minRange").setValue(range) { (err, _) in
            
            if err == nil
            {
                self.saveCurrentRangeQuestion(range: "1")
                completed(true)
            } else
            {
                print(err.debugDescription)
                completed(false)
            }
            
        }
        
        
        
        
    }
    
    //as date time
    func waitTimeDate () -> Date
    {
        let date = Date(timeIntervalSince1970: (self.waitTime! / 1000.0))
        return date
    }
    
    func syncDateTimeNow (synced : @escaping (Bool) -> Void)
    {
        Clock.sync(
            first: { date, offset in
                self.currentTime = date
                synced(true)
        })
}
}
