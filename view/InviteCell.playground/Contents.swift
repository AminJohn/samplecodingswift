//: Playground - noun: a place where people can play

import UIKit
import SwiftyVK

class InviteCell: UITableViewCell
{
    
    @IBOutlet weak var Avatar: RoundImage!
    
    @IBOutlet weak var FullName: UILabel!
    
    @IBOutlet weak var StatusIcon: UIImageView!
    
    @IBOutlet weak var StatusLabel: UILabel!
    
    @IBOutlet weak var SendButton: CornerRadiusButton!
    
    var IdUser : String!
    
    let inviteMessage = "Download App Enjou!"
    
    func setStatus(status : Int)
    {
        if (status == 0)
        {
            StatusIcon.image = #imageLiteral(resourceName: "Offline")
            StatusLabel.text = "Online"
        } else if (status == 1)
        {
            StatusIcon.image = #imageLiteral(resourceName: "Online")
            StatusLabel.text = "Offline"
        }
    }
    
    @IBAction func SendInvitation(_ sender: UIButton) {
        // в ожидании
        
        VK.API.Messages.send(([ .userId: IdUser!, .message: inviteMessage ])).onSuccess { (e) in
            
            let table = FriendTableVC()
            // Sent message
            DispatchQueue.main.async {
                table.tableView.reloadData()
                self.pendingButton(true)
            }
            DataService.instance.sendFriendReq(id: String(AccessData.instance.UserDATA!.id), friendId: self.IdUser)
            }
            .onError { print("SwiftyVK: friends.get failed with \n \($0)")}
            .send()
    }
    
    
    func pendingButton(_ isInvited : Bool)
    {
        if isInvited
        {
            self.SendButton.isUserInteractionEnabled = false
            self.SendButton.alpha = 0.5;
            self.SendButton.setTitle("Invited", for: .normal)
        } else
        {
            self.SendButton.isUserInteractionEnabled = true
            self.SendButton.alpha = 1;
            self.SendButton.setTitle("Invite", for: .normal)
        }
    }
    
    
}
