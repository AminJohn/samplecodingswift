import UIKit
import SwiftyVK
import SwiftyJSON
import Kingfisher
import Foundation
import Firebase

class FriendTableVC: UITableViewController, UISearchResultsUpdating{
    
    var filtered : [Items]?
    var searchController : UISearchController!
    
    //selected Profile to show in Uview Profile
    var profile : Items!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = 80
        
        self.navigationItem.title = "🤗InviteFriends"
        
        self.tableView.allowsSelection = false
        
        //Searc
        setUpBar()
        
        //fetch list invited friends
        DataService.instance.fetchInvitedList()
        
        
    }
    
    
    //MARK: show Search Bar
    func setUpBar ()
    {
        self.searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        navigationItem.searchController = searchController
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        
        self.filtered = AccessData.instance.OutAppFriends.filter({ (item : Items) -> Bool in
            
            if item.FullName().range(of: searchController.searchBar.text!) != nil
            {
                return true
            } else
            {
                return false
            }
        })
        
        self.tableView.reloadData()
        
    }
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        if searchController.isActive
        {
            return (filtered?.count)!
            
        } else
        {
            return (AccessData.instance.OutAppFriends.count)
        }
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if searchController.isActive
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InviteCell", for: indexPath) as! InviteCell
            // Configure the cell...
            cell.FullName.text = filtered![indexPath.row].FullName()
            // receive user image
            if let imageAdress = filtered![indexPath.row].photo_200
            {
                let imageURl = URL(string: imageAdress)
                cell.Avatar.kf.setImage(with: imageURl!)
                cell.Avatar.kf.indicatorType = .activity
            }
            cell.setStatus(status: filtered![indexPath.row].online!)
            cell.IdUser = (String)(filtered![indexPath.row].id!)
            
            // Condition for Invited Button
            if (AccessData.instance.InvitedList.contains(cell.IdUser))
            {
                cell.pendingButton(true)
            } else
            {
                cell.pendingButton(false)
            }
            
            return cell
        }  else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InviteCell", for: indexPath) as! InviteCell
            // Configure the cell...
            cell.FullName.text = (AccessData.instance.OutAppFriends[indexPath.row].FullName())
            // receive user image
            if let imageAdress = (AccessData.instance.OutAppFriends[indexPath.row].photo_200)
            {
                let imageURl = URL(string: imageAdress)
                cell.Avatar.kf.setImage(with: imageURl!)
                cell.Avatar.kf.indicatorType = .activity
            }
            
            cell.setStatus(status: (AccessData.instance.OutAppFriends[indexPath.row].online!))
            cell.IdUser = (String)(AccessData.instance.OutAppFriends[indexPath.row].id!)
            
            if (AccessData.instance.InvitedList.contains(cell.IdUser))
            {
                cell.pendingButton(true)
            } else
            {
                cell.pendingButton(false)
            }
            
            return cell
        }
        
    }
    
    //print(AccessData.instance.FilteredFriends?.description)
    
    
}
